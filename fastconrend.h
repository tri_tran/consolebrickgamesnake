#ifndef FASTCONREND_H
#define FASTCONREND_H

#include <windows.h>
#include <stdio.h>

#ifndef CONSOLECOLOR
#define CONSOLECOLOR
enum ConsoleColor
{
	BLACK=0,
	DARK_BLUE=1,
	DARK_GREEN=2,
	DARK_AQUA,DARK_CYAN=3,
	DARK_RED=4,
	DARK_PURPLE=5,DARK_PINK=5,DARK_MAGENTA=5,
	DARK_YELLOW=6,
	DARK_WHITE=7,
	GRAY=8,GREY=8,
	BLUE=9,
	GREEN=10,
	AQUA=11,CYAN=11,
	RED=12,
	PURPLE=13,PINK=13,MAGENTA=13,
	YELLOW=14,
	WHITE=15
};
#endif

struct _FCR_Window {
    HANDLE hStdOut;
    CHAR_INFO* outBuf;
    COORD wdSize, bufCoord;
    SMALL_RECT srctWriteRect;
    int color;
    int iw, outBufSize;
} FCR_window;

void FCR_initwindow(const wchar_t*, int, int, const wchar_t*, int, int, int);

void FCR_init();
void FCR_settitle(const wchar_t*);
void FCR_setsize(int, int);
void FCR_setfont(const wchar_t*, int, int);
void FCR_maximize();
void FCR_cleanup();

void FCR_gotoxy(int, int);
void FCR_clrscr();
int  FCR_textcolor();
int  FCR_backcolor();
void FCR_setcolor(int, int);
void FCR_settextcolor(int);
void FCR_setbackcolor(int);
void FCR_togglecursor(int);

void FCR_printf(const char*, ...);
void FCR_wprintf(const wchar_t*, ...);

void FCR_writebuffer();

#endif // FASTCONREND_H
