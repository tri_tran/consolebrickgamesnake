#ifndef HIGHRESTIMER_H
#define HIGHRESTIMER_H

#include <windows.h>

typedef struct HighResTimer {
    LARGE_INTEGER startTicks, frequency;
} high_res_timer;
void init_timer(high_res_timer*);
void start_timer(high_res_timer*);
double get_elapsed_time(high_res_timer*);

#endif
