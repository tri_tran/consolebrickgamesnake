#ifndef QUEUE_COORD_H
#define QUEUE_COORD_H

#include "valarray_coord.h"

typedef struct _queue_coord {
    valarray_coord nq;
    valarray_coord dq;
    int size;
} queue_coord;

void  queue_coord_init(queue_coord*);
void  queue_coord_enqueue(queue_coord*, COORD);
COORD queue_coord_dequeue(queue_coord*); //no throw
COORD queue_coord_front(const queue_coord*);
void  queue_coord_free(queue_coord*);

#endif // QUEUE_COORD_H
