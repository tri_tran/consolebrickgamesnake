#ifndef VALARRAY_COORD_H
#define VALARRAY_COORD_H

#include <stdlib.h>
#include <string.h>
#include <windows.h>

typedef struct _valarray_coord {
    COORD* data;
    int size;
    int cap;
} valarray_coord;

void  valarray_coord_init(valarray_coord*);
void  valarray_coord_reserve(valarray_coord*, int);
void  valarray_coord_push_back(valarray_coord*, COORD);
COORD valarray_coord_pop_back(valarray_coord*); //no throw
void  valarray_coord_free(valarray_coord*);

#endif // VALARRAY_COORD_H
